// 1.1osa.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <stack>
using namespace std;
void ringswitch(stack <int> &a,stack <int> &b,stack <int> &temp, int ring)
{
	if (ring == 0)
	{
		return;
	}
	else
	{
		ringswitch(a, temp, b, ring - 1);
		cout << a.top() << endl;
		b.push(a.top());
		a.pop();	
		ringswitch(temp, b, a, ring - 1);
	
	}
}


int main()
{
	setlocale(LC_ALL, "rus");
	stack <int> a;
	stack <int> b;
	stack <int> temp;
	int ring;
	cout << "Введите кол-во колец: " << endl;
	cin >> ring;
	// заполнение стека(1го стержня)
	for (int i = ring; i > 0; i--)
	{
		a.push(i);
	}
	if (b.empty())
		cout << "Стержень А(первый) заполнен\nСтержень B(конечный) пуст" << endl;
	ringswitch(a, b, temp, ring);
		cout << "После переноса колец: " << endl;
	if (a.empty())
		cout << "Стержень А(первый) пуст\nСтержень B(конечный) заполнен" << endl;
	return 0;

}

